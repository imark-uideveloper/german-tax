jQuery(document).ready(function () {
    (function ($) {
        'use strict';
        // Input FirstLetter Cap
        jQuery('.firstCap, textarea').on('keypress', function (event) {
            var jQuerythis = jQuery(this),
                thisVal = jQuerythis.val(),
                FLC = thisVal.slice(0, 1).toUpperCase(),
                con = thisVal.slice(1, thisVal.length);
            jQuery(this).val(FLC + con);
        });

        /* li Add Class Active*/
        $(function () {
            $(".welcome").slice(0, 1).show();
            $(".btn").on('click', function (e) {
                e.preventDefault();
                $(".welcome:hidden").slice(0, 1).slideDown();
                if ($(".welcome:hidden").length == 0) {}
                $('html,body').animate({
                    scrollTop: $(this).offset().top
                }, 1500);
            });
        });

        //Avoid pinch zoom on iOS
        document.addEventListener('touchmove', function (event) {
            if (event.scale !== 1) {
                event.preventDefault();
            }
        }, false);
    })(jQuery)
});

function progressBarScroll() {
    let winScroll = document.body.scrollTop || document.documentElement.scrollTop,
        height = document.documentElement.scrollHeight - document.documentElement.clientHeight,
        scrolled = (winScroll / height) * 100;
    document.getElementById("progressBar").style.width = scrolled + "%";
}

window.onscroll = function () {
    progressBarScroll();
};
/* Navigation ul li */
var selector = '.dashboard_nav ul li';
jQuery(selector).on('click', function () {
    jQuery(selector).removeClass('active');
    jQuery(this).addClass('active');
});